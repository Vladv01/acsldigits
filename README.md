#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
    FILE *fin=fopen("digits.in","r"), *fout=fopen("digits.out","w");
    int x,y,nr,cmax,cmin,s,cifprop,poz,pozc,c,p,med;
    for(int i=0;i<5;i++)
    {   cmax=0;
        nr=0;
        s=0;
        y=0;
        cmin=10;
        cifprop=0;
        poz=0;
        pozc=0;
        p=1;
        fscanf(fin,"%d \n",&x);
        int aux=x;
        do
        {
          s+=aux%10;
          if(cmax<aux%10)
            cmax=aux%10;
          if(cmin>aux%10)
            cmin=aux%10;
          nr++;
          aux/=10;

        }while(aux>0);
        med=s/nr;
        aux=x;
        while(aux>0)
        {
            poz++;
            if(aux%10>=cifprop && aux%10<=med)
            {
                pozc=poz;
                cifprop=aux%10;
            }
            aux/=10;
        }
        poz=0;
        s=s%10;
        while(x>0)
        {
            poz++;
            if(pozc==poz)
            {
                c=cifprop;
                if(c==0 || c==1 || c==2)
                    c=cmax;
                else
                    if(c==3 || c==4 || c==5)
                        c=cmin;
                    else
                         if(c==6 || c==7 || c==8)
                            c=s;
                         else
                            if(c==9)
                                c=0;
                y=y+c*p;
                p*=10;
            }
            else
            {
                y=y+x%10*p;
                p*=10;
            }
            x/=10;
        }
     fprintf(fout,"%d\n",y);
    }
    return 0;
}
